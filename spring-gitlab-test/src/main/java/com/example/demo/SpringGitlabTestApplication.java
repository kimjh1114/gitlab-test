package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGitlabTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGitlabTestApplication.class, args);
	}

}
