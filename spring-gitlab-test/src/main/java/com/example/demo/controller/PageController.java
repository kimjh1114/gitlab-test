package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

	@GetMapping("testpage") 
	public String index(Model model) {
		model.addAttribute("data", "test");
		return "testpage";
	}


}
